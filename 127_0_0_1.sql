-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2016 at 04:36 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bum-cakev3-v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `roles_users`
--

CREATE TABLE IF NOT EXISTS `roles_users` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_key` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles_users`
--

INSERT INTO `roles_users` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `status_users`
--

CREATE TABLE IF NOT EXISTS `status_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `status_users`
--

INSERT INTO `status_users` (`id`, `name`) VALUES
(1, 'activated'),
(2, 'banned'),
(3, 'deactivated'),
(4, 'email'),
(5, 'lock');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `username` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(55) DEFAULT '',
  `lastname` varchar(55) DEFAULT '',
  `unique_key` varchar(255) NOT NULL,
  `status_user_id` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `status_user_key` (`status_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `created`, `modified`, `username`, `email`, `password`, `firstname`, `lastname`, `unique_key`, `status_user_id`) VALUES
(1, '2016-04-18 14:03:27', '2016-04-19 01:25:23', 'username1', 'email1@yahoo.com', '$2y$10$zxbVYrhY1TIblTYCHLvCW.9S31e/IOFNgiEQGDkUI5zy/NRXfHsDa', '', '', 'c3b5169eacb6f20982a89d372124994842f498d4', 1),
(2, '2016-04-19 01:26:03', '2016-04-19 01:26:03', 'username2', 'email2@yahoo.com', '$2y$10$qqP7rAB9bRDIsGvNOt6giuPvSjh98MH6JgiV4dSDvNlX8l3crlpqm', '', '', '6c3959597445da5a2095623e33179ac6fc02f0f9', 1),
(3, '2016-05-02 03:21:40', '2016-05-02 03:21:40', 'username3', 'username3@yahoo.com', '$2y$10$K.4McogkLpD/ngV52EvIXOvpMSjyVmggzShnvPJWHsH4axn4VfNU6', '', '', 'd267a6508bf2639326c9d0efd4b2a90b401254ea', 1),
(4, '2016-05-02 04:45:17', '2016-05-02 04:45:17', 'username4', 'username4@yahoo.com', '$2y$10$hZa.URIJa507LPwOkB7Liesh4p8tbfYUbzrABc2/w1v8Nn102S5lK', '', '', '1dc9b8eba20299ba64521de86a38b19694d9dd75', 4),
(5, '2016-05-02 10:45:30', '2016-05-02 10:45:30', 'username5', 'username5@yahoo.com', '$2y$10$q1wYLAeALXFUaFSXn0QY7OLLderkduyWv0sXxT8QLUWIWmQ17LagW', '', '', '64b41cbbabfce0ef5ba14f832b61da30bece9fe6', 4),
(25, '2016-05-03 13:31:03', '2016-05-03 13:31:03', 'jmgcheng', 'jmgcheng@yahoo.com', '$2y$10$FBPgoytTuVWZcXipJdGiyeeDFrgIGim0HsQtxQIleC..nfbsPTTKC', '', '', 'd06b4bf049cffd83bf5e4212c52452cfcd5ac5ce', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `roles_users`
--
ALTER TABLE `roles_users`
  ADD CONSTRAINT `roles_users_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `roles_users_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`status_user_id`) REFERENCES `status_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
