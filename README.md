# CakePHP Application - BUM Skeleton

Cake skeleton with basic user management.

## Tasks To Do:
* database migration
* replace cake theme
* account profile page of currently logged-in user
* account profile image
* trappings base on current user status	
* links actions - show / hide - trapping user session
* deleting - should be disabled unless super admin?
* git ignore vendor, tmp, logs for finalize
* -

## App Capabilities:
* users - add
* users - edit
* users - view 
* users - delete
* users - login / logout
* users - registration with email sent
* users - register activation
* users - forgot password with email sent
* users - reset forgot password
* auth - authorize certain pages
* auth - allow certain page to specific roles
* status users - add
* status users - edit
* status users - view
* status users - delete
* roles - add
* roles - edit
* roles - view
* roles - delete
* register - confirm password
* -

## Current DB:
```
#!sql

CREATE TABLE status_users
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL
);
CREATE TABLE users 
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	created DATETIME,
	modified DATETIME,
	username VARCHAR(55) NOT NULL,
	email VARCHAR(55) NOT NULL,
	password VARCHAR(255) NOT NULL,
	firstname VARCHAR(55) DEFAULT '',
	lastname VARCHAR(55) DEFAULT '',
	unique_key VARCHAR(255) NOT NULL,
	status_user_id int(11) DEFAULT 1,
	FOREIGN KEY status_user_key (status_user_id) REFERENCES status_users(id)
);
CREATE TABLE roles
(
	id INT(11) AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(55) NOT NULL,
	UNIQUE KEY (name)
);
CREATE TABLE roles_users
(
	user_id INT(11) NOT NULL,
	role_id INT(11) NOT NULL,
	PRIMARY KEY (user_id, role_id),
    FOREIGN KEY user_key (user_id) REFERENCES users(id),
    FOREIGN KEY role_key (role_id) REFERENCES roles(id)
);

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User');
INSERT INTO `status_users` (`id`, `name`) VALUES
(1, 'activated'),
(2, 'banned'),
(3, 'deactivated'),
(4, 'email'),
(5, 'lock');

```

--------------------------------------------------


## Setup:
1. Clone repo
2. check 127_0_0_1.sql file and install db
3. Read and edit `config/app.php` and setup the 'Datasources' and any other configuration relevant for your application.
4. Run browser and check if installation is ok


--------------------------------------------------


## Setup - Fresh:
1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar create-project --prefer-dist cakephp/app [app_name]`.

If Composer is installed globally, run
```bash
composer create-project --prefer-dist cakephp/app [app_name]
```

3. Run browser and check if installation is ok
4. Create DB. Use schema provided
5. Read and edit `config/app.php` and setup the 'Datasources' and any other configuration relevant for your application.
6. Check if able to connect DB
7. Bake
```bash
bin/cake bake all
bin/cake bake all roles
bin/cake bake all status_users
bin/cake bake all users
```
8. Check default baked code functionalities if all are working
9. Slowly update code basing at Controllers, Mailers, Models, and Templates at 
[Sources](https://bitbucket.org/jmgcheng/bum_cakephp/src/209c7a7cf86b86a7f0dd3a04b456cfccc5bd7dae/src/?at=master)
10. Test all capabilities


--------------------------------------------------


## Notes:
* blank page at server? check server php version. Eleven2 `FcgidWrapper /usr/local/cpanel/cgi-sys/php56 .php` at .htaccess