<h1>
	Account Activation Required
</h1>
<p>
	You account is almost ready. 

	<?php
		if( isset($a_email_params['s_activation_url']) && !empty($a_email_params['s_activation_url']) ):
	?>

	Please <a href="<?php echo $a_email_params['s_activation_url']; ?>">activate it now</a>. Or visit this link: <?php echo $a_email_params['s_activation_url']; ?>

	<?php
		endif;
	?>
</p>


<br/>
<br/>


<p>
	Thanks, <br/>

</p>