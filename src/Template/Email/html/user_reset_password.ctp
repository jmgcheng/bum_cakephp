<h1>
	Reset Password Request
</h1>
<p>
	Someone requested for a reset password for your account. If this is you please follow the link below to reset your password.

	<br/>

	<?php
		if( isset($a_email_params['s_reset_pass_request_url']) && !empty($a_email_params['s_reset_pass_request_url']) ):
	?>

	<a href="<?php echo $a_email_params['s_reset_pass_request_url']; ?>">Reset Password</a>. Or visit this link: <?php echo $a_email_params['s_reset_pass_request_url']; ?>

	<?php
		endif;
	?>

	<br/>

	Kindly disregard this email if this was not you.


</p>


<br/>
<br/>


<p>
	Thanks, <br/>

</p>